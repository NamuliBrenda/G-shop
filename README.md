# G-shop

## Problem
People who live in remote areas or areas that are typically residential tend to have less or limited access 
to markets or even supermarkets where they can get groceries. Or some people are totally working class and 
buying groceries may be a burden to them because of their busy schedules.
<br>

As an individual who lives in a residential area, I believe that with technology people can be able to easily
access places where they can buy groceries or they can be delivered to their doors on lazy days.
<br>

### Solution
A platform that will allow vendors to market their products and at the same time give the customers access to 
these products and also provide them access to the nearest places where they can get groceries.

